const AWS = require('aws-sdk');
const ddb = new AWS.DynamoDB.DocumentClient();

exports.handler = function (request, response) {
    ddb.scan({
        TableName: "KChineseAnimal"
    }).promise()
        .then(data => {
            // your code goes here
            console.log(data);
        })
        .catch(err => {
            console.log(err);
            // error handling goes here
        });

    response.send({ "message": "Successfully executed" });
}